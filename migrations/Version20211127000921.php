<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211127000921 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE addon (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, description VARCHAR(255) DEFAULT NULL, price LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', status INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE addon_plan (addon_id INT NOT NULL, plan_id INT NOT NULL, INDEX IDX_81DCF78CCC642678 (addon_id), INDEX IDX_81DCF78CE899029B (plan_id), PRIMARY KEY(addon_id, plan_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE agency (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, address VARCHAR(255) DEFAULT NULL, city VARCHAR(40) DEFAULT NULL, state VARCHAR(50) DEFAULT NULL, zip VARCHAR(15) DEFAULT NULL, country VARCHAR(50) DEFAULT NULL, phone VARCHAR(50) DEFAULT NULL, email VARCHAR(60) DEFAULT NULL, website VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE client (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, address VARCHAR(255) DEFAULT NULL, city VARCHAR(40) DEFAULT NULL, state VARCHAR(50) DEFAULT NULL, zip VARCHAR(15) DEFAULT NULL, country VARCHAR(50) DEFAULT NULL, phone VARCHAR(50) DEFAULT NULL, email VARCHAR(60) DEFAULT NULL, website VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE data (id INT AUTO_INCREMENT NOT NULL, website_id INT DEFAULT NULL, addon_id INT DEFAULT NULL, agency_id INT DEFAULT NULL, client_id INT DEFAULT NULL, created_by_id INT NOT NULL, price DOUBLE PRECISION NOT NULL, payment_period VARCHAR(20) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_ADF3F36318F45C82 (website_id), INDEX IDX_ADF3F363CC642678 (addon_id), INDEX IDX_ADF3F363CDEADB2A (agency_id), INDEX IDX_ADF3F36319EB6921 (client_id), INDEX IDX_ADF3F363B03A8386 (created_by_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE plan (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, description VARCHAR(255) DEFAULT NULL, specifications LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', price LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE transaction (id INT AUTO_INCREMENT NOT NULL, website_id INT NOT NULL, created_by_id INT DEFAULT NULL, type VARCHAR(20) NOT NULL, amount DOUBLE PRECISION NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_723705D118F45C82 (website_id), INDEX IDX_723705D1B03A8386 (created_by_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE transaction_addon (transaction_id INT NOT NULL, addon_id INT NOT NULL, INDEX IDX_70B6413F2FC0CB0F (transaction_id), INDEX IDX_70B6413FCC642678 (addon_id), PRIMARY KEY(transaction_id, addon_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE website (id INT AUTO_INCREMENT NOT NULL, client_id INT NOT NULL, agency_id INT DEFAULT NULL, created_by_id INT NOT NULL, name VARCHAR(60) NOT NULL, description VARCHAR(255) DEFAULT NULL, subdomain VARCHAR(60) DEFAULT NULL, domain VARCHAR(150) DEFAULT NULL, created_at DATETIME NOT NULL, INDEX IDX_476F5DE719EB6921 (client_id), INDEX IDX_476F5DE7CDEADB2A (agency_id), INDEX IDX_476F5DE7B03A8386 (created_by_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE addon_plan ADD CONSTRAINT FK_81DCF78CCC642678 FOREIGN KEY (addon_id) REFERENCES addon (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE addon_plan ADD CONSTRAINT FK_81DCF78CE899029B FOREIGN KEY (plan_id) REFERENCES plan (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE data ADD CONSTRAINT FK_ADF3F36318F45C82 FOREIGN KEY (website_id) REFERENCES website (id)');
        $this->addSql('ALTER TABLE data ADD CONSTRAINT FK_ADF3F363CC642678 FOREIGN KEY (addon_id) REFERENCES addon (id)');
        $this->addSql('ALTER TABLE data ADD CONSTRAINT FK_ADF3F363CDEADB2A FOREIGN KEY (agency_id) REFERENCES agency (id)');
        $this->addSql('ALTER TABLE data ADD CONSTRAINT FK_ADF3F36319EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE data ADD CONSTRAINT FK_ADF3F363B03A8386 FOREIGN KEY (created_by_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE transaction ADD CONSTRAINT FK_723705D118F45C82 FOREIGN KEY (website_id) REFERENCES website (id)');
        $this->addSql('ALTER TABLE transaction ADD CONSTRAINT FK_723705D1B03A8386 FOREIGN KEY (created_by_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE transaction_addon ADD CONSTRAINT FK_70B6413F2FC0CB0F FOREIGN KEY (transaction_id) REFERENCES transaction (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE transaction_addon ADD CONSTRAINT FK_70B6413FCC642678 FOREIGN KEY (addon_id) REFERENCES addon (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE website ADD CONSTRAINT FK_476F5DE719EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE website ADD CONSTRAINT FK_476F5DE7CDEADB2A FOREIGN KEY (agency_id) REFERENCES agency (id)');
        $this->addSql('ALTER TABLE website ADD CONSTRAINT FK_476F5DE7B03A8386 FOREIGN KEY (created_by_id) REFERENCES user (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE addon_plan DROP FOREIGN KEY FK_81DCF78CCC642678');
        $this->addSql('ALTER TABLE data DROP FOREIGN KEY FK_ADF3F363CC642678');
        $this->addSql('ALTER TABLE transaction_addon DROP FOREIGN KEY FK_70B6413FCC642678');
        $this->addSql('ALTER TABLE data DROP FOREIGN KEY FK_ADF3F363CDEADB2A');
        $this->addSql('ALTER TABLE website DROP FOREIGN KEY FK_476F5DE7CDEADB2A');
        $this->addSql('ALTER TABLE data DROP FOREIGN KEY FK_ADF3F36319EB6921');
        $this->addSql('ALTER TABLE website DROP FOREIGN KEY FK_476F5DE719EB6921');
        $this->addSql('ALTER TABLE addon_plan DROP FOREIGN KEY FK_81DCF78CE899029B');
        $this->addSql('ALTER TABLE transaction_addon DROP FOREIGN KEY FK_70B6413F2FC0CB0F');
        $this->addSql('ALTER TABLE data DROP FOREIGN KEY FK_ADF3F36318F45C82');
        $this->addSql('ALTER TABLE transaction DROP FOREIGN KEY FK_723705D118F45C82');
        $this->addSql('DROP TABLE addon');
        $this->addSql('DROP TABLE addon_plan');
        $this->addSql('DROP TABLE agency');
        $this->addSql('DROP TABLE client');
        $this->addSql('DROP TABLE data');
        $this->addSql('DROP TABLE plan');
        $this->addSql('DROP TABLE transaction');
        $this->addSql('DROP TABLE transaction_addon');
        $this->addSql('DROP TABLE website');
    }
}
