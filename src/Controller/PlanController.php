<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use App\Form\PlanType;
use App\Entity\Plan;

class PlanController extends AbstractController
{
    /**
     * @Route("/plan/{name}", name="plan_display")
     */
    public function display($name): Response
    {
        $repo_plan = $this->getDoctrine()->getRepository(Plan::Class);
        $plan = $repo_plan->findOneByName($name);
        $form = $this->createForm(PlanType::class, $plan);
        return $this->render('plan/display.html.twig', [
            'plan' => $plan,
            'form' => $form->createView(),
        ]);
    }
    /**
     * @Route("/admin/plan/add", name="admin_plan_add")
     */
    public function add(): Response
    {
        $plan = new Plan;
        $form = $this->createForm(PlanType::class, $plan);
        return $this->render('plan/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
