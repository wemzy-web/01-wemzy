<?php

namespace App\Controller;

use App\Entity\User;
use App\Security\LoginFormAuthenticator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

class RegistrationController extends AbstractController
{
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder, GuardAuthenticatorHandler $guardHandler, LoginFormAuthenticator $authenticator, \Swift_Mailer $mailer): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);
    
        if ($form->isSubmitted()) {

            $rep_user=$this->getDoctrine()->getRepository(User::class);
            if($rep_user->findByEmail($_POST['email'])) {
                $this->addFlash("error","Cette adresse e-mail est déjà utilisé par une autre personne.");
                return $this->redirectToRoute("register");
            }

            $user->setFirstname($_POST['fname']);
            $user->setLastname($_POST['lname']);
            $user->setRoles(['ROLE_STARTUP']);
            $user->setEmail($_POST['email']);

            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $_POST['password']
                )
            );
            $user->setCreated(new \DateTime('now'));
            $user->setConfirmed(0);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            $message = (new \Swift_Message('Confirm Registration'))
            ->setFrom('noreply@call.cnrst.ma', 'Plateform de Gestion de Projets')
            ->setTo($user->getEmail())
            ->setBody(
                $this->renderView(
                    'emails/welcome.html.twig',
                    [
                    'name' => $user->getFirstname(),
                    'token'=> $user->getToken()
                    ]
                )
            );
            $mailer->send($message);

            $this->addFlash("success","Votre compte a été créé, un lien de le confirmation vous a été envoyé. ");
            $this->addFlash("spam","IMPORTANT : Si vous ne recevez pas de message de confirmation sur votre boite, merci de <b>voir dans le dossier SPAM (COURRIER INDESIRABLE)</b> ");
            return $this->redirectToRoute("login");
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }

    public function validate(Request $request, $token): Response
    {
        $rep_user=$this->getDoctrine()->getRepository(User::class);

        if ($user=$rep_user->findOneByToken($token)) {

            $user->setConfirmed(1);
            $user->setConfirmedOn(new \DateTime());
            $user->setToken(null);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            
            $this->addFlash("success","Votre compte a été confirmé. Vous pouvez vous connecter ci-dessous.");
            return $this->redirectToRoute("login");
        }

        $this->addFlash("error","Votre code validation est incorrect ou votre compte a déjà été validé.");
        return $this->redirectToRoute("login");
}
}
